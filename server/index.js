const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

const cars = require('./routes/cars')
app.use('/cars', cars)

module.exports = app