const app = require('./index.js')
const express = require('express');
const path = require('path');

app.use(express.static(path.join(__dirname, '../app/build')));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../app/build/index.html'));
});
app.listen(process.env.SERVER_PORT || 3001, (err) => {
    if (err) throw err
    console.log(`Server is running on http://127.0.0.1:3001`)
})