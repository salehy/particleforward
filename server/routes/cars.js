const express = require('express')
const router = express.Router()

const carService = require('../services/cars.services')

router.get('/', carService.getCars)

module.exports = router
