import React from 'react';

const Loading = () => {
    return (
        <div className="flex flex-col w-screen h-screen bg-gray-100 items-center justify-center">
            <span className="text-xl font-light text-gray-500">
                Loading
            </span>
        </div>
    );
};

export default Loading;