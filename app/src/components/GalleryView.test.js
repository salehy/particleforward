import axios from 'axios';
jest.mock('axios');
describe('fetchData', () => {
    it('fetch API data', async () => {
        axios.get.mockImplementationOnce(() => Promise.resolve('http://localhost:3001/cars'));
    });

    it('test fetch error', async () => {
        const errorMessage = 'network err';

        axios.get.mockImplementationOnce(() =>
            Promise.reject(new Error(errorMessage)),
        );
    });
});