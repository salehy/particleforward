import React from 'react';

const Header = () => {
    return (
        <div className="flex flex-col bg-blue-700 p-4  w-full items-center shadow-md">

            <div className="flex flex-row w-full sm:w-full md:w-2/3 space-x-2">
                <img src="/assets/logo.svg" className="h-14 " alt="" />

            </div>
        </div>
    );
};

export default Header;