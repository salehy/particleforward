import React from 'react';

const ImageLoader = () => {
    return (
        <div className="text-gray-200 text-base font-light">
            loading
        </div>
    )
}
export default ImageLoader