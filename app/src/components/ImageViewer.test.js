import React from 'react';
import renderer from 'react-test-renderer';
import ImageViewer from './ImageViewer';

it('renders correctly', () => {
    const tree = renderer
        .create(<ImageViewer title="car title" closeViewer={() => console.log('testing image viewer ')} selectedItemUri={'https://particleforward.com/api/challenge/assets/image1'} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});