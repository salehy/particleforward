import React, { useState } from 'react';
import ImageLoader from './ImageLoader';

const ImageViewer = ({ closeViewer, title, selectedItemUri }) => {
    const [isLoading, setIsLoading] = useState(true)

    return (
        <div className="flex flex-col absolute h-screen w-screen bg-gray-600 bg-opacity-60 top-0 left-0 items-center justify-center">

            <div className="flex flex-col w-full sm:w-full md:w-2/3  h-full sm:h-full md:h-4/5 bg-white p-2 rounded-lg space-y-4 justify-start ">
                <div className="flex flex-row justify-between items-center w-full h-20 p-4">
                    <div />
                    <span className="text-2xl font-medium text-gray-700"> {title}</span>
                    <div className="flex flex-col rounded-full bg-gray-200 cursor-pointer h-8 w-8 items-center justify-center p-2 text-gray-600 hover:bg-gray-300 hover:shadow " onClick={() => closeViewer()}> X </div>
                </div>
                <div className="flex flex-col h-full w-full items-center justify-center">
                    <img src={`${selectedItemUri}_27.jpg`} className=" object-contain " alt="" onLoad={() => setIsLoading(false)}
                    />
                    {
                        isLoading && <div className="flex flex-col   items-center justify-center animate-pulse "><ImageLoader /> </div>
                    }

                </div>
            </div>
        </div>
    );
};

export default ImageViewer;