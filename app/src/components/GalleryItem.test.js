import React from 'react';
import renderer from 'react-test-renderer';
import GalleryItem from './GalleryItem';

it('renders correctly', () => {
    const tree = renderer
        .create(<GalleryItem item={{
            "uri": "https://particleforward.com/api/challenge/assets/image1",
            "set": "fe4cfedffdffffffff"
        }} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});