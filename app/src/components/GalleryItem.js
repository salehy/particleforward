import React, { useState } from 'react';
import ImageLoader from './ImageLoader';
const GalleryItem = ({ item }) => {
    const [isLoading, setIsLoading] = useState(true)
    return (
        <div className={`flex flex-col rounded w-full sm:w-full md:w-52  h-64 sm:h-64 md:h-40 bg-white shadow scale-100 transition-transform  transform duration-150 ease-out hover:shadow-md hover:scale-105 cursor-pointer mr-0 sm:mr-0 md:mr-4 mb-4 p-2   `}>
            <img src={`${item.uri}_2.jpg`} className={`w-full h-full  object-contain transition-transform  transform duration-300  ease-in  ${isLoading ? "scale-105 opacity-0 bg-gray-200" : "  bg-white transition-opacity scale-100 opacity-100"}`} alt=""
                onLoad={() => setIsLoading(false)}
            />
            {
                isLoading && <div className="flex flex-col   items-center justify-center animate-pulse "><ImageLoader /> </div>
            }
        </div>
    );
};

export default GalleryItem;

