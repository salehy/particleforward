import axios from 'axios';
import React, { useEffect, useState } from 'react';
import GalleryItem from './GalleryItem';
import ImageViewer from './ImageViewer'

const GalleryView = () => {
    const [openImageViewer, setOpenImageViewer] = useState(false)
    const [data, setData] = useState();
    const [selectedItemUri, setSelectedItemUri] = useState('')
    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                'http://localhost:3001/cars',
            );

            result.status === 200 && setData(result?.data?.cars)
        };

        fetchData();
    }, []);
    const handleViewImageOnSelectedItem = (uri) => {
        setOpenImageViewer(true)
        setSelectedItemUri(uri)
    }
    return (

        <div className="flex flex-wrap    items-start justify-start p-4 w-full sm:w-full md:w-2/3  ">


            {
                data?.images?.map((item, index) => <div key={index} onClick={() => handleViewImageOnSelectedItem(item.uri)} className=" flex w-full sm:w-full md:w-auto">
                    <GalleryItem item={item} />
                </div>
                )
            }
            {
                openImageViewer && <ImageViewer title={data.title} closeViewer={() => { setOpenImageViewer(false); setSelectedItemUri('') }} selectedItemUri={selectedItemUri} />
            }
        </div>
    );
};

export default GalleryView;