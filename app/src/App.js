import React, { Suspense } from 'react';
import Header from './components/Header'
import Loading from './components/Loading';
const App = () => {
  const GalleryView = React.lazy(() => import('./components/GalleryView'));
  return (
    <Suspense fallback={<Loading />}>
      <div className=" flex flex-col items-center bg-gray-100 w-screen h-screen overflow-y-auto">
        <Header />
        <GalleryView />
      </div>
    </Suspense>
  );
}

export default App;
