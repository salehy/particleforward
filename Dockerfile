FROM node:13.12.0-alpine AS ui-app
WORKDIR /usr/src/app
COPY app/ ./app/
RUN cd app && npm install && npm run build

FROM node:13.12.0-alpine
WORKDIR /root/
COPY --from=ui-app /usr/src/app/app/build ./app/build
COPY server/package*.json ./server/
RUN cd server && npm install
COPY server/ ./server/

EXPOSE 3001

CMD ["node", "./server/server.js"]